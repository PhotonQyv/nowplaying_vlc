#!/usr/bin/env python3.8

"""
 Copyright (C) 2019  PhotonQyv

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import logging
import dbus
import gi.repository.GLib
import stackprinter

from dbus.mainloop.glib import DBusGMainLoop
from mastodon import Mastodon, MastodonError
from pydbus import SessionBus, Variant, bus
from urllib import parse
from argparse import ArgumentParser

stackprinter.set_excepthook(style="color")


# Get VLC D-Bus object
def get_vlc() -> bus.Bus:
    return session_bus.get("org.mpris.MediaPlayer2.vlc", "/org/mpris/MediaPlayer2")


# Get Notifications D-Bus object
def get_notifications() -> bus.Bus:
    return session_bus.get(".Notifications", "/org/freedesktop/Notifications")


# Actually do the status posting to Mastodon
def post_to_mastodon(album: str, artist: str, artfile: str, output: bytes):
    try:

        mastodon = Mastodon(
            access_token="/home/qyv/Documents/Important/np_vlc-keys/np_vlc_usercred.secret",
            api_base_url="https://mastodon.xyz/",
        )

        if artfile:

            logging.debug(f"About to post: {artfile}")

            result = mastodon.media_post(
                artfile, description=f"Cover image of '{album}' by {artist}"
            )

            logging.debug(f"{result=}")

            mastodon.status_post(
                output, visibility="unlisted", language="eng", media_ids=result
            )

        else:

            mastodon.status_post(output, visibility="unlisted", language="eng")

    except MastodonError as err:

        logging.error(f"Mastodon error:  {err}")


# Callback that checks for Notifications from VLC
def do_check(bus: bus.Bus, message: dbus.lowlevel.Message):

    client, one, two, nowplaying, *rest = message.get_args_list()

    logging.debug(f"{client}: '{one}' : '{two}' : 'np: {nowplaying}'")
    # logging.debug(f"{rest}")

    # So VLC likes to post DBus notifications with
    #  'VLC media player' as the first argument, when
    #  the playlist is full of locally sourced files, but
    #  if you're streaming it posts with 'vlc' as the
    #  first argument. BUT, if you're locally sourcing it
    #  also posts 'vlc' notifications. The logic below
    #  should prevent multiple Mastodon status updates
    #  for the same file.

    if client == "VLC media player" and nowplaying == "Now Playing":

        logging.debug("do_toot(VLC media player)")

        do_toot()

    elif client == "vlc" and nowplaying != "VLC media player":

        logging.debug("do_toot(vlc)")

        do_toot()


# Method that does the heavy lifting
def do_toot():

    output = r""

    vlc = get_vlc()
    notifications = get_notifications()

    data = vlc.Metadata

    artist = title = url = nowplaying = album = art_url = art_file = ""

    if "xesam:artist" in data.keys():
        artist, *_ = data["xesam:artist"]

    if "xesam:title" in data.keys():
        title = data["xesam:title"]

    if "xesam:album" in data.keys():
        album = data["xesam:album"]

    if "xesam:url" in data.keys():
        url = data["xesam:url"]

    if "vlc:nowplaying" in data.keys():
        nowplaying = data["vlc:nowplaying"]

    # Not every album has a cover image...
    if "mpris:artUrl" in data.keys():

        art_url = data["mpris:artUrl"]

        # Since artUrl is actually a URI
        #  it needs to be parsed and 'unquoted'
        #  to be usable for the call to Mastodon.
        #
        art_file = parse.unquote(parse.urlparse(art_url).path)

        logging.debug(f"{art_url=}\n{art_file=}")

    # Streamed music tracks don't!'
    else:

        art_file = ""

    logging.debug(
        f"{artist=}\n{title=}\n{album=}\n{url=}\n{art_url=}\n{nowplaying=}"
    )

    # If we're streaming from a radio station for example.
    if artist == "" and nowplaying != "":

        if (
            nowplaying.startswith("  -")
            or nowplaying == "THIS STATION WILL CONTINUE AFTER THIS BREAK"
        ):

            output = ""

            pass

        output = f"#NowPlaying\n\n{nowplaying}\n\n#{title}\n\n#nowplaying #VLC "

        # Optional url output
        if title == "VANTARADIO":

            output += f"\n\n({url})"

        else:

            output += "(stream)"

        output = output.encode("utf8")

    # Otherwise we _must_ (current understanding) be playing
    #  tracks that are locally sourced.
    elif artist != "":

        output = (
            f"#NowPlaying\n\n{artist} - {title} - {album}\n\n#nowplaying #VLC (local)"
        )

        output = output.encode("utf8")

    logging.debug(f"output: {output}")

    if output != "":
        # Post the status to Mastodon
        logging.info("post_to_mastodon")
        post_to_mastodon(album, artist, art_file, output)

    output = ""
    hints = {}

    # Locally sourced route.
    if nowplaying == "" and artist != "" and title != "":

        output += f"Now Playing: {artist}\n{title}"

        if art_url != "":

            # hints in Notify() is an dictionary with
            #  a str key and Variant value (in this case a str).
            hints = {"image-path": Variant.new_string(art_url)}

        else:

            hints = {}

    # Streaming route.
    elif nowplaying != "":

        output += f"Now Playing: {nowplaying}"

        hints = {}

    try:

        logging.debug(f"{output=}")

        if output != "":

            notifications.Notify(
                "VLC Tooter", 0, "", "#NowPlaying...", output, [], hints, 5000,
            )

    except Exception as e:

        logging.error(e)


if __name__ == "__main__":

    mainloop = None

    # Default to INFO level of logging
    log_level = logging.INFO

    parser = ArgumentParser()

    parser.description = "Post what's playing in VLC to Mastodon."

    parser.add_argument("-d", "--debug", help="turn debugging on", action="store_true")

    args = parser.parse_args()

    if args.debug:

        log_level = logging.DEBUG

    else:

        log_level = logging.INFO

    logging.basicConfig(level=log_level, format="%(message)s")

    try:

        DBusGMainLoop(set_as_default=True)

        session_bus = (
            SessionBus()
        )  # Use pydbus version for everything since it's easier to work with.

        # Use dbus-python for the listening for notifications,
        #  since I've not yet worked out how to do the eavesdropping in pydbus.
        note_bus = dbus.SessionBus()

        note_bus.add_match_string_non_blocking(
            "eavesdrop=true, interface='org.freedesktop.Notifications', member='Notify'",
        )

        note_bus.add_message_filter(do_check)

        mainloop = gi.repository.GLib.MainLoop()
        mainloop.run()

    except KeyboardInterrupt:

        print()
        mainloop.quit()

    except Exception as err:

        logging.error(err)
        mainloop.quit()
